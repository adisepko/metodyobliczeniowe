"""
Usage:
  integral.py  (-f) INPUT

Options:
    -h --help       Show help.
    --version       Show version.
    -f              Read points from file

Arguments:
    INPUT file or polynomial depends on -f or -s option being selected

"""
import sys

from docopt import docopt


def calculate_integral(points):
    u_integral = underflow_integral(points)
    o_integral = overflow_integral(points)
    t_integral = trapeze_integral(points)
    s_integral = simpson_integral(points)
    avg = (u_integral + o_integral + t_integral + s_integral)/4
    print(
        f'Metoda prostokątów z niedomiarem: {u_integral} błąd bezwzględny: {abs(u_integral - avg)} błąd względny: {round(abs((u_integral - avg) / u_integral), 2)}%')
    print(
        f'Metoda prostokątów z nadmiarem: {o_integral} błąd bezwzględny: {abs(o_integral - avg)} błąd względny: {round(abs((o_integral - avg) / o_integral), 2)}%')
    print(
        f'Metoda trapezów: {t_integral} błąd bezwzględny: {abs(t_integral - avg)} błąd względny: {round(abs((t_integral - avg) / t_integral), 2)}%')
    print(
        f'Metoda Simpsona: {s_integral} błąd bezwzględny: {abs(s_integral - avg)} błąd względny: {round(abs((s_integral - avg) / s_integral), 2)}%')


def underflow_integral(points):
    step = points[1][0] - points[0][0]

    sum = 0
    for i, point in enumerate(points):
        if i != len(points) - 1:
            sum += point[1]
    return sum * step


def trapeze_integral(points):
    step = points[1][0] - points[0][0]
    sum = (points[0][1] + points[len(points) - 1][1]) / 2
    for i, point in enumerate(points):
        if i != len(points) - 1 and i != 0:
            sum += point[1]
    return sum * step


def simpson_integral(points):
    step = points[1][0] - points[0][0]
    sum = 0
    for i, point in enumerate(points):
        coefficient = get_coefficients(i, len(points))
        sum += coefficient * point[1]
    return sum * step / 3


def get_coefficients(i, length):
    if i == 0 or i == length - 1:
        return 1
    elif i == length - 2:
        return 4
    elif i % 2 == 0:
        return 2
    else:
        return 4


def overflow_integral(points):
    step = points[1][0] - points[0][0]
    sum = 0
    for i, point in enumerate(points):
        if i != 0:
            sum += point[1]
    return sum * step


def read_points_from_file(file_path):
    with open(file_path, "r") as f:
        for line in f:
            split = line.replace("(", "").replace(")", "").split(" ")
            yield list(map(lambda x: tuple(map(float, x.split(','))), split))


def print_matrix(matrix):
    for row in matrix:
        print(row)


def main():
    arguments = docopt(__doc__, options_first=True)
    input_string = arguments.get("INPUT")
    if arguments.get("-f"):
        points = read_points_from_file(input_string)
        for point in points:
            calculate_integral(point)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    sys.exit(main())
