"""
Usage:
  approximation.py  (-f) INPUT LEVEL (-d)
  approximation.py  (-f) INPUT LEVEL

Options:
    -h --help       Show help.
    --version       Show version.
    -f              Read points from file
    -d              debug

Arguments:
    INPUT file or polynomial depends on -f or -s option being selected

"""
import sys

from docopt import docopt

from pretty_print import print_polynomial


def print_approximation(point_series, level, debug):
    m = level + 1
    sk, tk = calculate_vectors(m, point_series)
    s_matrix = calculate_s_matrix(sk, m)
    inverted_matrix = calculate_inverted_matrix(s_matrix)
    if debug:
        # 1x^3+ 0,5x^2 -.1x -.1
        print(point_series)
        print("sk vector")
        print_matrix(sk)
        print('tk vector')
        print_matrix(tk)
        print("s_matrix:")
        print_matrix(s_matrix)
        print("inverted matrix:")
        print_matrix(inverted_matrix)
    polynomial = multiply_matrix(inverted_matrix, tk, debug)
    polynomial.reverse()
    print_polynomial(polynomial)


def calculate_inverted_matrix(s_matrix):
    result = []
    det = calculate_det(s_matrix)
    complementary_matrix = get_complementary_matrix(s_matrix)
    trans_matrix = matrix_transposition(complementary_matrix)
    for i in range(len(trans_matrix)):
        result.append([])
        for element in trans_matrix[i]:
            result[i].append(element / det)
    return result


def multiply_matrix(s, t, debug):
    result = []
    for row in s:
        tmp = 0
        for i in range(len(row)):
            element = row[i] * t[i]
            if debug:
                print(f' {row[i]} * {t[i]} = {element}')
            tmp = tmp + element
        if debug:
            print(f'sum = {tmp}\n')
        result.append(tmp)
    return result


def matrix_transposition(matrix):
    length = len(matrix)
    result = []
    for i in range(length):
        result.append([])
        for j in range(length):
            result[i].append(matrix[j][i])
    return result


def get_complementary_matrix(s_matrix):
    m = -1
    complementary_matrix = []
    r = range(len(s_matrix))
    for i in r:
        complementary_matrix_row = []
        for j in r:
            power = ((i + 1) + (j + 1))
            minor = calculate_minor(s_matrix, i, j)
            det = calculate_det(minor)
            complementary_matrix_row.append(round(m ** power * det, 2))
        complementary_matrix.append(complementary_matrix_row)
    return complementary_matrix


def calculate_det(s_matrix):
    det = 0
    if len(s_matrix) == 2:
        return s_matrix[0][0] * s_matrix[1][1] - s_matrix[1][0] * s_matrix[0][1]
    for j in range(0, len(s_matrix)):
        minor = calculate_minor(s_matrix, 0, j)
        det = det + s_matrix[j][0] * ((-1) ** (j + 2) * calculate_det(minor))
    return det


def deepcopy(table):
    copy = []
    for row in table:
        copy.append(row.copy())
    return copy


def calculate_minor(matrix, i, j):
    matrix_copy = deepcopy(matrix)
    for row in matrix_copy:
        del row[i]
    del matrix_copy[j]
    return matrix_copy


def calculate_s_matrix(sk, level):
    s_matrix = []
    for i in range(level):
        s_matrix.append(sk[i:level + i])
    return s_matrix


def print_matrix(matrix):
    for row in matrix:
        print(row)
    print('')


def calculate_vectors(level, point_series):
    s = []
    t = []
    for k in range(0, level * 2-1):
        sk = 0
        tk = 0
        for x, y in point_series:
            x_k = x ** k
            sk = sk + x_k
            tk = tk + x_k * y
        s.append(sk)
        if len(t) < level:
            t.append(tk)
    return s, t


def read_points_from_file(file_path):
    with open(file_path, "r") as f:
        for line in f:
            split = line.replace("(", "").replace(")", "").split(" ")
            yield list(map(lambda x: tuple(map(float, x.split(','))), split))


def main():
    arguments = docopt(__doc__)
    input_string = arguments.get("INPUT")
    if arguments['-f']:
        points = read_points_from_file(input_string)
        for point_series in points:
            print_approximation(point_series, int(arguments.get("LEVEL")), arguments['-d'])


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    sys.exit(main())
