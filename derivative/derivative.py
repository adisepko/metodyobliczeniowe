"""
Usage:
  derivative.py  (-f) INPUT POINT

Options:
    -h --help       Show help.
    --version       Show version.
    -f              Read points from file

Arguments:
    INPUT file or polynomial depends on -f or -s option being selected
    POINT point to calculate derivative1

"""
import sys

from docopt import docopt

from central_difference import central_difference
from normal_difference import normal_difference
from backwards_difference import backwards_difference


def calculate_derivative(points, point):
    avg = 0
    counter = 0
    normal_diff = 0
    backwards_diff = 0
    central_diff = 0
    print(points)
    for i, y in enumerate(points):
        print(f'punkt: {y[0]}')
        if i != len(points) - 1:
            normal_diff = normal_difference(points, y[0])
            print(f'Metoda różnicy zwykłej: {normal_diff} ')
            avg += normal_diff
            counter += 1
        if i != 0:
            backwards_diff = backwards_difference(points, y[0])
            print(f'Metoda różnicy wstecznej: {backwards_diff} ')
            avg += backwards_diff
            counter += 1
        if i != len(points) - 1 and i != 0:
            central_diff = central_difference(points, float(y[0]))
            print(f'Metoda różnicy centralnej: {central_diff} ')
            avg += central_diff
            counter += 1
        avg = avg / counter
        if normal_diff != 0:
            print(
                f'Metoda różnicy zwykłej błąd bezwzględny: {abs(normal_diff - avg)} błąd względny: {abs((normal_diff - avg) / normal_diff)}')
        if backwards_diff != 0:
            print(
                f'Metoda różnicy wstecznej błąd bezwzględny: {abs(backwards_diff - avg)} błąd względny: {abs((backwards_diff - avg) / backwards_diff)}')
        if central_diff != 0:
            print(
                f'Metoda różnicy centralnej błąd bezwzględny: {abs(normal_diff - avg)} błąd względny: {abs((normal_diff - avg) / normal_diff)}')
        avg = 0
        counter = 0
        normal_diff = 0
        backwards_diff = 0
        central_diff = 0


def read_points_from_file(file_path):
    with open(file_path, "r") as f:
        for line in f:
            split = line.replace("(", "").replace(")", "").split(" ")
            yield list(map(lambda x: tuple(map(float, x.split(','))), split))


def print_matrix(matrix):
    for row in matrix:
        print(row)


def main():
    arguments = docopt(__doc__, options_first=True)
    input_string = arguments.get("INPUT")
    if arguments.get("-f"):
        points = read_points_from_file(input_string)
        for point in points:
            calculate_derivative(point, arguments.get("POINT"))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    sys.exit(main())
