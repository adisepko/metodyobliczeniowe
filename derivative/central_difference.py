def central_difference(points, point):
    diff_matrix = []
    l = expand_vector(list(map(lambda x: x[0], points)))
    index = l.index(float(point))
    step = points[1][0] - points[0][0]
    diff_table = calculate_diff(points)
    diff_matrix.append(diff_table)
    calculate_diff_rec(diff_matrix)
    length = calculate_length(points)
    diff_matrix = list(map(lambda x: append_zeros(x, length), diff_matrix))
    tmp_sum = 0
    result = [f"["]
    for i, vector in enumerate(diff_matrix):
        if i % 2 == 0:
            avg = (vector[index - 1] + vector[index + 1]) / 2
            tmp_sum = round(tmp_sum + (avg * factor(i + 1)), 8)
            result.append(
                f' {factor(i + 1)} * {avg} '
                f'{"+" if (len(diff_matrix) - 2) > i else ""} ')
    result.append(f"]/{round(step, 8)}\n")
    # print(''.join(result))
    return tmp_sum / step


def calculate_length(points):
    return len(points) * 2 - 1


def append_zeros(vector, length):
    result = []
    for i in range(len(vector)):
        result.append(0)
        result.append(vector[i])
    result.append(0)
    for i in range(int((length - len(result)) / 2)):
        result.insert(0, 0)
        result.insert(len(result), 0)
    return result


def expand_vector(vector):
    result = []
    for i in range(len(vector)):
        result.append(vector[i])
        if i < len(vector) - 1:
            result.append(0)
    return result


def calculate_diff(points):
    diff_table = []
    for j, (x, y) in enumerate(points):
        if j < len(points) - 1:
            diff_table.append(round(points[j + 1][1] - y, 8))
    return diff_table


def calculate_diff_rec(diff_matrix):
    points = diff_matrix[len(diff_matrix) - 1]
    diff_table = []
    for j, (y) in enumerate(points):
        if j < len(points) - 1:
            diff_table.append(round(points[j + 1] - y, 8))
    diff_matrix.append(diff_table)
    if any(el != 0 for el in diff_table) and len(diff_table) > 1:
        calculate_diff_rec(diff_matrix)


def factor(i):
    nominator = 1
    stop = int((i + 1) / 2) - 1
    for j in range(stop):
        nominator = nominator * -((j + 1)*(j+1))
    fac = factorial(i)
    return nominator / fac


def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)


def print_matrix(matrix):
    for row in matrix:
        print(row)
    print('')
