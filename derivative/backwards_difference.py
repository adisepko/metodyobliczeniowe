def backwards_difference(points, point):
    diff_matrix = []
    index = list(map(lambda x: x[0], points)).index(float(point))
    step = points[1][0] - points[0][0]
    diff_table = calculate_diff(points)
    diff_matrix.append(diff_table)
    calculate_diff_rec(diff_matrix)
    for vector in diff_matrix:
        append_zeros(vector, len(points))
    tmp_sum = 0
    result = [f"["]
    for i, vector in enumerate(diff_matrix):
        if len(vector) > index:
            degree = i + 1
            tmp_sum = round(tmp_sum + 1 / degree * vector[index], 8)
            result.append(
                f' {round( 1 / degree * vector[index], 8)} '
                f'{"+" if (i+1) != len(diff_matrix) else ""} ')
    result.append(f"]/{round(step, 8)}\n")
    # print(''.join(result))
    return tmp_sum / step


def append_zeros(vector, length):
    for i in range(length - len(vector)):
        vector.insert(0, 0)


def calculate_diff(points):
    diff_table = []
    for j, (x, y) in enumerate(points):
        if j > 0:
            diff_table.append(round(y - points[j - 1][1], 8))
    return diff_table


def calculate_diff_rec(diff_matrix):
    points = diff_matrix[len(diff_matrix) - 1]
    diff_table = []
    for j, (y) in enumerate(points):
        if j > 0:
            diff_table.append(round(y - points[j - 1], 8))
    diff_matrix.append(diff_table)
    if any(el != 0 for el in diff_table) and len(diff_table) > 1:
        calculate_diff_rec(diff_matrix)


def print_matrix(matrix):
    for row in matrix:
        print(row)
    print('')
