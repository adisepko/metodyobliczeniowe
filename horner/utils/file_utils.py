def read_coefficients_from_file_second_format(file_path):
    coefficients = []
    with open(file_path, "r") as f:
        for line in f:
            coefficients.append(line.replace(",", "."))
    return coefficients


def read_polynomial_from_file(file_path):
    with open(file_path, "r") as f:
        for line in f:
            yield line.replace(",", ".").split()
