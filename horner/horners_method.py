"""
Usage:
  horners_method.py  (-f|-s|-d) INPUT

Options:
    -h --help       Show help.
    --version       Show version.
    -f              Read polynomials from file (one polynomial per line)
    -d              Read polynomials from file (one coefficient per line)
    -s              Read polynomials from stdin

Arguments:
    INPUT file or polynomial depends on -f or -s option being selected

"""
import sys

from docopt import docopt

from utils.file_utils import read_polynomial_from_file, read_coefficients_from_file_second_format


def evaluate_polynomial(polynomial, x):
    b = 0
    for a in polynomial:
        b = float(a) + b * float(x)
    return round(b, 4)


def main():
    arguments = docopt(__doc__)
    input_string = arguments.get("INPUT")
    if arguments['-f']:
        polynomials = read_polynomial_from_file(input_string)
        for poly in polynomials:
            print(evaluate_polynomial(poly, poly.pop()))
    elif arguments['-d']:
        poly = read_coefficients_from_file_second_format(input_string)
        print(evaluate_polynomial(poly, poly.pop()))
    elif arguments['-s']:
        poly = input_string.split()
        print(evaluate_polynomial(poly, poly.pop()))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    sys.exit(main())
