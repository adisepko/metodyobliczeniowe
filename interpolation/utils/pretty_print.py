def get_superscript(x):
    normal = "0123456789"
    sub_s = "⁰¹²³⁴⁵⁶⁷⁸⁹"
    res = x.maketrans(''.join(normal), ''.join(sub_s))
    return x.translate(res)


def print_polynomial(polynomial):
    poly_len = len(polynomial)
    result = []
    for i, x in enumerate(polynomial):
        power = poly_len - i
        if x != 0:
            value = f"+{round(x, 2)}" if round(x, 2) > 0 and i != 0 else round(x, 2)
            if power > 1:
                result.append(f"{value}x{get_superscript(str(power))}")
            else:
                result.append(f"{value} ")
    print(''.join(result))
