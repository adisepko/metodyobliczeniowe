def read_points_from_file(file_path):
    with open(file_path, "r") as f:
        for line in f:
            split = line.replace("(", "").replace(")", "").split(" ")
            yield list(map(lambda x: tuple(map(float, x.split(','))), split))


def read_points_from_file_second_format(file_path):
    points = []
    with open(file_path, "r") as f:
        for line in f:
            point = tuple(map(float, line.replace(",", ".").split()))
            points.append(point)
    return points
