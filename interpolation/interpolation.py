"""
Usage:
  interpolation.py  (-f|-d|-s) INPUT

Options:
    -h --help       Show help.
    --version       Show version.
    -f              Read points from file (all points in one line comma separated)
    -d            Read points from file (one point per line)
    -s              Read points from stdin

Arguments:
    INPUT file or polynomial depends on -f or -s option being selected

"""
import sys
from operator import add
from docopt import docopt

from utils.file_utils import read_points_from_file, read_points_from_file_second_format
from utils.pretty_print import print_polynomial


def lagrange(points):
    b = [0] * len(points)
    for i in range(len(points)):
        b = list(map(add, b, calculate_fraction(i, points)))
    return b


def calculate_fraction(i, points):
    numerator = None
    denominator = 1
    x0, y0 = points[i]
    for j, (x, y) in enumerate(points):
        if i != j:
            numerator = calculate_numerator(numerator, x)
            denominator = denominator * (x0 - x)
    return list(map(lambda z: y0 * z / denominator, numerator))


def calculate_numerator(result, x):
    if result is None:
        result = [1, -x]
    else:
        result = multiply(result, -x)
    return result


def multiply(current_result, next_element):
    current_result.append(0)
    result_copy = current_result.copy()
    result_copy[0] = 0
    for i, el in enumerate(current_result):
        if i < len(current_result) - 1:
            result_copy[i + 1] = el * next_element
    return list(map(add, current_result, result_copy))


def main():
    arguments = docopt(__doc__)
    input_string = arguments.get("INPUT")
    if arguments['-f']:
        points = read_points_from_file(input_string)
        for point_series in points:
            print_polynomial(lagrange(point_series))
    elif arguments['-d']:
        points = read_points_from_file_second_format(input_string)
        print_polynomial(lagrange(points))
    elif arguments['-s']:
        point_series = list(map(lambda x: tuple(map(float, x.split(','))), input_string))
        print_polynomial(lagrange(point_series))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    sys.exit(main())
